from php:7.3-fpm

run  echo "nameserver 1.1.1.1" > /etc/resolv.conf && apt update && apt install nginx git vim curl  procps zip unzip curl openssl git libzip-dev libonig-dev libxml2-dev zlib1g-dev libpng-dev libjpeg-dev libwebp-dev libfreetype6-dev  zlib1g-dev  -y
run echo "nameserver 1.1.1.1" > /etc/resolv.conf && curl --resolve 1.1.1.1 -sS https://getcomposer.org/installer -o composer-setup.php && php composer-setup.php --install-dir=/usr/local/bin --filename=composer
run docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-webp-dir=/usr/include/
run docker-php-ext-install pdo mbstring  bcmath gd zip mysqli pdo_mysql exif soap

run composer global require hirak/prestissimo
env COMPOSER_MEMORY_LIMIT=-1

workdir /var/www/html
copy ./myapp /var/www/html

run if [ ! -f ./auth.json ]; then echo "{\"github-oauth\": {\"github.com\": \"763fc6f9916feda6690fe487e0f1e8bdc23c6d1e\"} }" > ./auth.json ; fi
run composer install 
run composer dump-autoload

run chown -R www-data:www-data /var/www/html/storage

