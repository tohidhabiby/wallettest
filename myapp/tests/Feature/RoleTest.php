<?php

namespace Tests\Feature;

use App\Constants\PermissionTitle;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleTest extends TestCase
{
    /**
     * @test
     */
    public function createRole()
    {
        $title = $this->faker->name;
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_ROLE);
        $response = $this->postJson(
            route('roles.store'),
            [Role::TITLE => $title]
        )->assertCreated();
        $this->assertEquals($response->getOriginalContent()->getTitle(), $title);
    }
    /**
     * @test
     */
    public function createRoleAndAssignPermissions()
    {
        $title = $this->faker->name;
        $permission = Permission::factory()->create();
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_ROLE);
        $response = $this->postJson(
            route('roles.store'),
            [
                Role::TITLE => $title,
                'permissions' => [$permission->getId()]
            ]
        )->assertCreated();
        $this->assertTrue($response->getOriginalContent()->permissions->contains($permission));
    }

    /**
     * @test
     */
    public function userCanNotCreateRoleWithoutPermission()
    {
        $title = $this->faker->name;
        $this->actingAsUser();
        $response = $this->postJson(
            route('roles.store'),
            [Role::TITLE => $title]
        )->assertForbidden();
    }

    /**
     * @test
     */
    public function UserWithoutLoggedInCanNotCreateUser()
    {
        $role = Role::factory()->make();
        $this->postJson(
            'api/roles',
            [Role::TITLE => $role->{Role::TITLE}]
        )->assertUnauthorized();
    }

    /**
     * @test
     */
    public function canNotCreateRoleWithInvalidParameters()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_ROLE);
        $response = $this->postJson('api/roles', [Role::TITLE => null]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(Role::TITLE, $response->json());

        $role = Role::factory()->create();
        $response = $this->postJson(route('roles.store'), [Role::TITLE => $role->{Role::TITLE}]);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertArrayHasKey(Role::TITLE, $response->json());
    }

    /**
     * @test
     */
    public function getRole()
    {
        $role = Role::factory()->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ROLE);
        $response = $this->getJson(route('roles.show', $role));
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($role));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetRole()
    {
        $role = Role::factory()->create();
        $this->actingAsUser();
        $this->getJson(route('roles.show', $role))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotGetRole()
    {
        $role = Role::factory()->create();
        $this->getJson(route('roles.show', $role))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function updateRole()
    {
        $this->actingAsUserWithPermission(PermissionTitle::UPDATE_ROLE);
        $title = $this->faker->unique()->name;

        $role = Role::factory()->create();
        $response = $this->patchJson(
            route('roles.update', $role),
            [Role::TITLE => $title]
        );
        $response->assertOk();

        $this->assertEquals($title, $response->getOriginalContent()->{Role::TITLE});
    }

    /**
     * @test
     */
    public function updateRoleAndAssignPermission()
    {
        $this->actingAsUserWithPermission(PermissionTitle::UPDATE_ROLE);
        $role = Role::factory()->create();
        $permission = Permission::factory()->create();
        $response = $this->patchJson(
            route('roles.update', $role),
            [
                Role::TITLE => $role->getTitle(),
                'permissions' => [$permission->getId()],
            ]
        );
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->permissions->contains($permission));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotUpdateRole()
    {
        $this->actingAsUser();
        $title = $this->faker->unique()->name;

        $role = Role::factory()->create();
        $response = $this->patchJson(
            route('roles.update', $role),
            [Role::TITLE => $title]
        );
        $response->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotUpdateRole()
    {
        $title = $this->faker->unique()->name;

        $role = Role::factory()->create();
        $response = $this->patchJson(
            route('roles.update', $role),
            [Role::TITLE => $title]
        );
        $response->assertUnauthorized();
    }

    /**
     * @test
     */
    public function deleteRole()
    {
        $this->actingAsUserWithPermission(PermissionTitle::DELETE_ROLE);
        $role = Role::factory()->create();
        $this->deleteJson(route('roles.destroy', $role));
        $this->assertDatabaseMissing(Role::TABLE, [Role::ID => $role->{Role::ID}]);
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotDeleteRole()
    {
        $role = Role::factory()->create();
        $this->deleteJson(route('roles.destroy', $role))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotDeleteRole()
    {
        $this->actingAsUser();
        $role = Role::factory()->create();
        $this->deleteJson(route('roles.destroy', $role))->assertForbidden();
    }

    /**
     * @test
     */
    public function getAllRolePagination()
    {
        Role::factory()->count(8)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_ROLES);
        $response = $this->getJson(route('roles.index'));
        $response->assertOk();
        $this->assertEquals(9, count($response->getOriginalContent()));
    }

    /**
     * @test
     */
    public function getAllRolePaginationPerpage()
    {
        Role::factory()->count(5)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_ROLES);
        $response = $this->getJson(route('roles.index') . '?per_page=3');
        $response->assertOk();
        $this->assertEquals(3, count($response->getOriginalContent()));
    }

    /**
     * @test
     */
    public function filterRoleByNationalCode()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_ROLES);
        Role::factory()->count(5)->create();
        $role = Role::factory()->create([Role::TITLE => '1234567891']);
        $secondRole = Role::factory()->create([Role::TITLE => '1234567892']);
        $response = $this->getJson(route('roles.index') . '?title=1234567891');
        $response->assertOk();
        $this->assertEquals(1, count($response->getOriginalContent()));
        $this->assertTrue($response->getOriginalContent()->contains(Role::ID, $role->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(Role::ID, $secondRole->getId()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterRoleByNationalCode()
    {
        $this->actingAsUser();
        Role::factory()->count(5)->create();
        Role::factory()->create([Role::TITLE => '1234567891']);
        $this->getJson(route('roles.index') . '?title=1234567891')->assertForbidden();
    }

}
