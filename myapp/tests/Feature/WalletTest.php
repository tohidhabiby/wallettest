<?php

namespace Tests\Feature;

use App\Constants\PermissionTitle;
use App\Models\Wallet;
use Illuminate\Http\Response;
use Tests\TestCase;

class WalletTest extends TestCase
{
    /**
     *  @test
     */
    public function createWallet()
    {
        $wallet = Wallet::factory()->make();

        $this->actingAsUserWithPermission(PermissionTitle::CREATE_WALLET);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::TYPE => $wallet->getType(),
                Wallet::NAME => $wallet->getName()
            ]
        )->assertCreated();

        $this->assertEquals($response->getOriginalContent()->getName(), $wallet->getName());
        $this->assertEquals($response->getOriginalContent()->getType(), $wallet->getType());
    }

    /**
     * @test
     */
    public function userCanNotCreateWalletWithoutPermission()
    {
        $this->actingAsUser();
        $this->postJson(route('wallets.store'), [])->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotCreateWallet()
    {
        $this->postJson(route('wallets.store'), [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanNotCreateWalletWithNullParameters()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_WALLET);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::TYPE => null,
                Wallet::NAME => null,
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(Wallet::NAME, $response->json());
        $this->assertArrayHasKey(Wallet::TYPE, $response->json());

        $this->actingAsUserWithPermission(PermissionTitle::CREATE_WALLET);
        $response = $this->postJson(
            route('wallets.store'),
            [
                Wallet::TYPE => 'sample text',
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertArrayHasKey(Wallet::TYPE, $response->json());
    }

    /**
     * @test
     */
    public function getWallet()
    {
        $wallet = Wallet::factory()->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_WALLET);
        $response = $this->getJson(route('wallets.show',$wallet));
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($wallet));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetWallet()
    {
        $wallet = Wallet::factory()->create();
        $this->actingAsUser();
        $this->getJson(route('wallets.show',$wallet))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotGetWallet()
    {
        $wallet = Wallet::factory()->create();
        $this->getJson(route('wallets.show',$wallet))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function updateWallet()
    {
        $user = $this->actingAsUserWithPermission(PermissionTitle::UPDATE_WALLET);
        $wallet = Wallet::factory()->create([Wallet::USER_ID => $user->getId()]);
        $secondWallet = Wallet::factory()->make();

        $response = $this->patchJson(
            route('wallets.update', $wallet),
            [
                Wallet::TYPE => $secondWallet->getType(),
                Wallet::NAME => $secondWallet->getName()
            ]
        );
        $response->assertOk();

        $this->assertEquals($secondWallet->getName(), $response->getOriginalContent()->getName());
        $this->assertEquals($secondWallet->getType(), $response->getOriginalContent()->getType());
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotUpdateWallet()
    {
        $this->actingAsUser();
        $wallet = Wallet::factory()->create();
        $this->patchJson(route('wallets.update',$wallet), [])->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotUpdateWallet()
    {
        $wallet = Wallet::factory()->create();
        $this->patchJson(route('wallets.update', $wallet), [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function deleteWallet()
    {
        $this->actingAsUserWithPermission(PermissionTitle::DELETE_WALLET);
        $wallet = Wallet::factory()->create();
        $this->deleteJson(route('wallets.destroy',$wallet));
        $this->assertDatabaseMissing(Wallet::TABLE,[Wallet::ID => $wallet->getId()]);
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotDeleteWallet()
    {
        $wallet = Wallet::factory()->create();
        $this->deleteJson(route('wallets.destroy',$wallet))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotDeleteWallet()
    {
        $this->actingAsUser();
        $wallet = Wallet::factory()->create();
        $this->deleteJson(route('wallets.destroy',$wallet))->assertForbidden();
    }

    /**
     * @test
     */
    public function getAllWalletPagination()
    {
        Wallet::factory()->count(8)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_WALLETS);
        $response = $this->getJson(route('wallets.index'));
        $response->assertOk();

        $this->assertEquals(8, count($response->getOriginalContent()) );
    }

    /**
     * @test
     */
    public function getAllWalletPaginationPerPage()
    {
        Wallet::factory()->count(5)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_WALLETS);
        $response = $this->getJson(route('wallets.index') . '?per_page=3');
        $response->assertOk();
        $this->assertEquals(3,count($response->getOriginalContent()));
    }
}
