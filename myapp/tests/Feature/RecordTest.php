<?php

namespace Tests\Feature;

use App\Constants\PermissionTitle;
use App\Models\Record;
use App\Models\Wallet;
use Illuminate\Http\Response;
use Tests\TestCase;

class RecordTest extends TestCase
{
    /**
     *  @test
     */
    public function createRecord()
    {
        $user = $this->actingAsUserWithPermission(PermissionTitle::CREATE_RECORD);
        $wallet = Wallet::factory()->create([Wallet::USER_ID => $user->getId()]);
        $record = Record::factory()->make([Record::WALLET_ID => $wallet->getId()]);
        $response = $this->postJson(
            route('records.store'),
            [
                Record::WALLET_ID => $record->getWalletId(),
                Record::TYPE => $record->getType(),
                Record::AMOUNT => $record->getAmount()
            ]
        )->assertCreated();

        $this->assertEquals($response->getOriginalContent()->getWalletId(), $record->getWalletId());
        $this->assertEquals($response->getOriginalContent()->getType(), $record->getType());
        $this->assertEquals($response->getOriginalContent()->getAmount(), $record->getAmount());
    }

    /**
     * @test
     */
    public function userCanNotCreateRecordWithoutPermission()
    {
        $this->actingAsUser();
        $this->postJson(route('records.store'), [])->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotCreateRecord()
    {
        $this->postJson(route('records.store'), [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userCanNotCreateRecordWithNullParameters()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_RECORD);
        $response = $this->postJson(
            route('records.store'),
            [
                Record::WALLET_ID => null,
                Record::TYPE => null,
                Record::AMOUNT => null,
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(Record::WALLET_ID, $response->json());
        $this->assertArrayHasKey(Record::TYPE, $response->json());
        $this->assertArrayHasKey(Record::AMOUNT, $response->json());
    }

    /**
     * @test
     */
    public function userCanNotCreateRecordWithInvalidWallet()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_RECORD);
        $record = Record::factory()->create();
        $response = $this->postJson(
            route('records.store'),
            [
                Record::WALLET_ID => $record->getWalletId(),
                Record::TYPE => 'sample text',
                Record::AMOUNT => 'sample text',
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertArrayHasKey(Record::WALLET_ID, $response->json());
        $this->assertArrayHasKey(Record::TYPE, $response->json());
    }

    /**
     * @test
     */
    public function getRecord()
    {
        $record = Record::factory()->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_RECORD);
        $response = $this->getJson(route('records.show',$record));
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($record));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetRecord()
    {
        $record = Record::factory()->create();
        $this->actingAsUser();
        $this->getJson(route('records.show',$record))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotGetRecord()
    {
        $record = Record::factory()->create();
        $this->getJson(route('records.show',$record))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function updateRecord()
    {
        $user = $this->actingAsUserWithPermission(PermissionTitle::UPDATE_RECORD);
        $wallet = Wallet::factory()->create([Wallet::USER_ID => $user->getId()]);
        $record = Record::factory()->create([Record::WALLET_ID => $wallet->getId()]);
        $secondWallet = Wallet::factory()->create([Wallet::USER_ID => $user->getId()]);
        $secondRecord = Record::factory()->make([Record::WALLET_ID => $secondWallet->getId()]);

        $response = $this->patchJson(
            route('records.update', $record),
            [
                Record::WALLET_ID => $secondRecord->getWalletId(),
                Record::TYPE => $secondRecord->getType(),
                Record::AMOUNT => $secondRecord->getAmount()
            ]
        );
        $response->assertOk();

        $this->assertEquals($secondRecord->getWalletId(), $response->getOriginalContent()->getWalletId());
        $this->assertEquals($secondRecord->getType(), $response->getOriginalContent()->getType());
        $this->assertEquals($secondRecord->getAmount(), $response->getOriginalContent()->getAmount());
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotUpdateRecord()
    {
        $this->actingAsUser();
        $record = Record::factory()->create();
        $this->patchJson(route('records.update',$record), [])->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotUpdateRecord()
    {
        $record = Record::factory()->create();
        $this->patchJson(route('records.update', $record), [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function deleteRecord()
    {
        $this->actingAsUserWithPermission(PermissionTitle::DELETE_RECORD);
        $record = Record::factory()->create();
        $this->deleteJson(route('records.destroy',$record));
        $this->assertDatabaseMissing(Record::TABLE,[Record::ID => $record->getId()]);
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotDeleteRecord()
    {
        $record = Record::factory()->create();
        $this->deleteJson(route('records.destroy',$record))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotDeleteRecord()
    {
        $this->actingAsUser();
        $record = Record::factory()->create();
        $this->deleteJson(route('records.destroy',$record))->assertForbidden();
    }

    /**
     * @test
     */
    public function getAllRecordPagination()
    {
        Record::factory()->count(8)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_RECORDS);
        $response = $this->getJson(route('records.index'));
        $response->assertOk();

        $this->assertEquals(8, count($response->getOriginalContent()) );
    }
}
