<?php

namespace Tests\Feature;

use App\Constants\PermissionTitle;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * @test
     */
    public function createUser()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_USER);
        $user = User::factory()->make();
        $response = $this->postJson(
            'api/users',
            [
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::PASSWORD => $user->{User::PASSWORD},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
            ]
        );

        $response->assertCreated();
        $this->assertDatabaseHas(User::TABLE, $user->toArray());
    }

    /**
     * @test
     */
    public function createUserAndAssignRoles()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_USER);
        $user = User::factory()->make();
        $role = Role::factory()->create();
        $response = $this->postJson(
            'api/users',
            [
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::PASSWORD => $user->{User::PASSWORD},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
                'roles' => [$role->getId()],
            ]
        );

        $response->assertCreated();
        $this->assertTrue($response->getOriginalContent()->roles->contains($role));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotCreate()
    {
        $this->actingAsUser();
        $user = User::factory()->make();
        $this->postJson(
            'api/users',
            [
                User::FIRST_NAME => $user->{User::FIRST_NAME},
                User::LAST_NAME => $user->{User::LAST_NAME},
                User::PASSWORD => $user->{User::PASSWORD},
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $user->{User::CHARGE},
            ]
        )->assertForbidden();
    }

    /**
     * @test
     */
    public function UserWithoutLoggedInCanNotCreateUser()
    {
        $this->postJson('api/users', [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function canNotCreateUserWithInvalidParameters()
    {
        $this->actingAsUserWithPermission(PermissionTitle::CREATE_USER);
        $response = $this->postJson(
            'api/users',
            [
                User::EMAIL => null,
                User::LAST_NAME => null,
                User::FIRST_NAME => null,
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(User::EMAIL, $response->json());
        $this->assertArrayHasKey(User::FIRST_NAME, $response->json());
        $this->assertArrayHasKey(User::LAST_NAME, $response->json());

        $user = User::factory()->create();
        $response = $this->postJson(
            route('users.store'),
            [
                User::EMAIL => $user->{User::EMAIL},
                User::CHARGE => $this->faker->title,
                User::FIRST_NAME => $this->faker->paragraphs,
            ]
        );
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertArrayHasKey(User::EMAIL, $response->json());
        $this->assertArrayHasKey(User::CHARGE, $response->json());
        $this->assertArrayHasKey(User::FIRST_NAME, $response->json());
    }

    /**
     * @test
     */
    public function getUser()
    {
        $user = User::factory()->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_USER);
        $response = $this->getJson(route('users.show', $user));
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->is($user));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetUser()
    {
        $user = User::factory()->create();
        $this->actingAsUser();
        $this->getJson(route('users.show', $user))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotGetUser()
    {
        $user = User::factory()->create();
        $this->getJson(route('users.show', $user))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function updateUser()
    {
        $this->actingAsUserWithPermission(PermissionTitle::UPDATE_USER);
        $firstName = $this->faker->firstName;
        $lastName = $this->faker->lastName;
        $email = $this->faker->email;

        $user = User::factory()->create();
        $response = $this->patchJson(
            route('users.update', $user),
            [
                User::FIRST_NAME => $firstName,
                User::EMAIL => $email,
                User::LAST_NAME => $lastName,
            ]
        );
        $response->assertOk();
        $this->assertEquals($lastName, $response->getOriginalContent()->{User::LAST_NAME});
        $this->assertEquals($firstName, $response->getOriginalContent()->{User::FIRST_NAME});
        $this->assertEquals($email, $response->getOriginalContent()->{User::EMAIL});
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotUpdateUser()
    {
        $this->actingAsUser();
        $user = User::factory()->create();
        $this->patchJson(route('users.update', $user), [])->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotUpdateUser()
    {
        $user = User::factory()->create();
        $this->patchJson(route('users.update', $user), [])->assertUnauthorized();
    }

    /**
     * @test
     */
    public function deleteUser()
    {
        $this->actingAsUserWithPermission(PermissionTitle::DELETE_USER);
        $user = User::factory()->create();
        $this->deleteJson(route('users.destroy', $user));
        $this->assertDatabaseMissing(User::TABLE, [User::ID => $user->{User::ID}]);
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotDeleteUser()
    {
        $this->actingAsUser();
        $user = User::factory()->create();
        $this->deleteJson(route('users.destroy', $user))->assertForbidden();
    }

    /**
     * @test
     */
    public function userWithoutLoggedInCanNotDeleteUser()
    {
        $user = User::factory()->create();
        $this->deleteJson(route('users.destroy', $user))->assertUnauthorized();
    }

    /**
     * @test
     */
    public function getAllUserPagination()
    {
        User::factory()->count(9)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        $response = $this->getJson(route('users.index'));
        $response->assertOk();
        $this->assertEquals(10, count($response->getOriginalContent()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetAllUserPagination()
    {
        User::factory()->count(9)->create();
        $this->actingAsUser();
        $this->getJson(route('users.index'))->assertForbidden();
    }

    /**
     * @test
     */
    public function getAllUserPaginationPerpage()
    {
        User::factory()->count(5)->create();
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        $response = $this->getJson(route('users.index') . '?per_page=3');
        $response->assertOk();
        $this->assertEquals(3, count($response->getOriginalContent()));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotGetAllUserPaginationPerpage()
    {
        User::factory()->count(5)->create();
        $this->actingAsUser();
        $this->getJson(route('users.index') . '?per_page=3')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUsersByFirstName()
    {
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::FIRST_NAME => 'testttttt']);
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        $response = $this->getJson(route('users.index') . '?firstName=testtt');
        $response->assertOk();
        $this->assertTrue($response->getOriginalContent()->first()->is($user));
    }

    /**
     * @test
     */
    public function userWithoutPermissionCanNotFilterUsersByFirstName()
    {
        User::factory()->count(5)->create();
        $user = User::factory()->create([User::FIRST_NAME => 'testttttt']);
        $this->actingAsUser();
        $this->getJson(route('users.index') . '?firstName=testtt')->assertForbidden();
    }

    /**
     * @test
     */
    public function filterUserByLastName()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(7)->create();
        $user = User::factory()->create([User::LAST_NAME => 'tttttttttttttt']);
        $secondUser = User::factory()->create([User::LAST_NAME => 'mmmm']);
        $response = $this->getJson(route('users.index') . '?lastName=ttttttttt');
        $response->assertOk();
        $this->assertEquals(1, count($response->getOriginalContent()));
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }

    /**
     * @test
     */
    public function filterUserByEmail()
    {
        $this->actingAsUserWithPermission(PermissionTitle::GET_ALL_USERS);
        User::factory()->count(4)->create();
        $user = User::factory()->create([User::EMAIL => 'example@example.com']);
        $secondUser = User::factory()->create([User::EMAIL => 'kjidfjdierkf']);
        $response = $this->getJson(route('users.index') . '?email=xamp');
        $response->assertOk();
        $this->assertEquals(1, count($response->getOriginalContent()));
        $this->assertTrue($response->getOriginalContent()->contains(User::ID, $user->getId()));
        $this->assertFalse($response->getOriginalContent()->contains(User::ID, $secondUser->getId()));
    }
}
