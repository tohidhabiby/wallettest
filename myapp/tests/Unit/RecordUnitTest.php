<?php

namespace Tests\Unit;

use App\Interfaces\Models\RecordInterface;
use App\Models\Record;
use App\Models\Wallet;
use Tests\TestCase;

class RecordUnitTest extends TestCase
{
    /**
     * @test
     */
    public function createRecord()
    {
        $record = Record::factory()->make();

        $createRecord = Record::createObject(
            $record->wallet,
            $record->getAmount(),
            $record->getType()
        );

        $this->assertTrue($createRecord instanceof RecordInterface);

        $this->assertEquals($createRecord->getWalletId(), $record->getWalletId());
        $this->assertEquals($createRecord->getType(), $record->getType());
        $this->assertEquals($createRecord->getAmount(), $record->getAmount());
    }

    /**
     * @test
     */
    public function updateRecord()
    {
        $record = Record::factory()->create();
        $secondRecord = Record::factory()->make();
        $updatedRecord = $record->updateObject(
            $secondRecord->wallet,
            $secondRecord->getAmount(),
            $secondRecord->getType()
        );

        $this->assertTrue($updatedRecord instanceof RecordInterface);
        $this->assertDatabaseHas(
            Record::TABLE,
            [
                Record::WALLET_ID => $secondRecord->getWalletId(),
                Record::ID => $record->getId(),
                Record::AMOUNT => $secondRecord->getAmount(),
                Record::TYPE => $secondRecord->getType(),
            ]
        );
    }
}
