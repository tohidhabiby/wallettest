<?php

namespace Tests\Unit;

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Tests\TestCase;
use App\Interfaces\Models\UserInterface;

class UserUnitTest extends TestCase
{
    /**
     * @test
     */
    public function testCreateUser()
    {
        $user = User::factory()->make();
        $createdUser = User::createObject(
            $user->{User::FIRST_NAME},
            $user->{User::LAST_NAME},
            $user->{User::PASSWORD},
            $user->{User::EMAIL},
            $user->{User::CHARGE}
        );
        $this->assertTrue($createdUser instanceof UserInterface);
        $this->assertEquals($createdUser->{User::FIRST_NAME}, $user->{User::FIRST_NAME});
        $this->assertEquals($createdUser->{User::LAST_NAME}, $user->{User::LAST_NAME});
    }

    /**
     * @test
     */
    public function updateUser()
    {
        $user = User::factory()->create();
        $firstName = $this->faker->firstName;
        $lastName = $this->faker->lastName;
        $email = $this->faker->email;
        $charge = (string)$this->faker->randomDigit;
        $password = '123';
        $createdUser = $user->updateObject(
            $firstName,
            $lastName,
            $password,
            $email,
            $charge
        );

        $this->assertTrue($createdUser instanceof UserInterface);
        $this->assertDatabaseHas(
            User::TABLE,
            [
                User::ID => $user->getId(),
                User::EMAIL => $email,
                User::FIRST_NAME => $firstName,
                User::LAST_NAME => $lastName,
                User::CHARGE => (string)$charge,
                User::PASSWORD => $password,
            ]
        );
    }

    /**
     * @test
     */
    public function checkHasPermission()
    {
        /** @var User $user */
        $user = User::factory()->create();
        $permission = Permission::factory()->create();
        $role=Role::factory()->create();
        $user->roles()->attach($role);
        $role->permissions()->attach($permission);
        $this->assertTrue($user->hasPermission($permission->getTitle()));
        $secondPermission = Permission::factory()->create();
        $this->assertFalse($user->hasPermission($secondPermission));
    }
}
