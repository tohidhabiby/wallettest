<?php

namespace Tests\Unit;

use App\Interfaces\Models\RoleInterface;
use App\Models\Role;
use Tests\TestCase;

class RoleUnitTest extends TestCase
{
    /**
     * @test
     */
    public function testCreateRole()
    {
        $role = Role::factory()->make();
        $createdRole = Role::createObject($role->{Role::TITLE});
        $this->assertTrue($createdRole instanceof RoleInterface);
        $this->assertEquals($createdRole->{Role::TITLE}, $role->{Role::TITLE});
    }

    /**
     * @test
     */
    public function updateRole()
    {
        $role = Role::factory()->create();
        $title = $role->getTitle();
        $createdRole = $role->updateObject($title);

        $this->assertTrue($createdRole instanceof RoleInterface);
        $this->assertDatabaseHas(
            Role::TABLE,
            [Role::ID => $role->getId(), Role::TITLE => $title]
        );
    }
}
