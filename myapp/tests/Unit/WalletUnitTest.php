<?php

namespace Tests\Unit;

use App\Interfaces\Models\WalletInterface;
use App\Models\Wallet;
use Tests\TestCase;

class WalletUnitTest extends TestCase
{
    /**
     * @test
     */
    public function createWallet()
    {
        $wallet = Wallet::factory()->make();

        $createWallet = Wallet::createObject(
            $wallet->user,
            $wallet->getName(),
            $wallet->getType()
        );

        $this->assertTrue($createWallet instanceof WalletInterface);

        $this->assertEquals($createWallet->getUserId(), $wallet->getUserId());
        $this->assertEquals($createWallet->getType(), $wallet->getType());
        $this->assertEquals($createWallet->getName(), $wallet->getName());
    }

    /**
     * @test
     */
    public function updateWallet()
    {
        $wallet = Wallet::factory()->create();
        $secondWallet = Wallet::factory()->make();

        $updatedWallet = $wallet->updateObject(
            $secondWallet->getName(),
            $secondWallet->getType()
        );

        $this->assertTrue($updatedWallet instanceof WalletInterface);
        $this->assertDatabaseHas(
            Wallet::TABLE,
            [
                Wallet::USER_ID => $wallet->getUserId(),
                Wallet::ID => $wallet->getId(),
                Wallet::NAME => $secondWallet->getName(),
                Wallet::TYPE => $secondWallet->getType(),
            ]
        );
    }
}
