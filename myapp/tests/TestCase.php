<?php

namespace Tests;

use App\Http\Middleware\PermissionMiddleware;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use WithFaker;
    use RefreshDatabase;
    use CreatesApplication;

    /**
     * @return User
     */
    public function actingAsUser()
    {
        $loggedInUser = User::factory()->create();
        Passport::actingAs($loggedInUser);

        return $loggedInUser;
    }

    /**
     * @param string $permission Permission.
     *
     * @return User
     */
    public function actingAsUserWithPermission(string $permission)
    {
        $loggedInUser = User::factory()->create();
        Passport::actingAs($loggedInUser);
        $role = Role::factory()->create();
        $loggedInUser->roles()->attach($role);
        $permissionModel = Permission::whereTitleIs($permission)->first();
        if (!$permissionModel) {
            Permission::insert([Permission::TITLE => $permission]);
            $permissionModel = Permission::whereTitleIs($permission)->first();
        }
        $role->permissions()->sync([$permissionModel->getId()]);

        return $loggedInUser;
    }
}
