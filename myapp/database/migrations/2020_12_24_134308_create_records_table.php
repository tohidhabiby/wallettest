<?php

use App\Models\Record;
use App\Models\Wallet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Record::TABLE, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(Record::WALLET_ID);
            $table->unsignedBigInteger(Record::AMOUNT);
            $table->enum(Record::TYPE, Record::$types)->comment(implode(',', Record::$types));
            $table->timestamps();

            $table->foreign(Record::WALLET_ID)->references(Wallet::ID)->on(Wallet::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Record::TABLE);
    }
}
