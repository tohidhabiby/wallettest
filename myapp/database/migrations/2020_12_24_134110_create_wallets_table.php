<?php

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Wallet::TABLE, function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger(Wallet::USER_ID);
            $table->string(Wallet::NAME);
            $table->enum(Wallet::TYPE, Wallet::$types)->comment(implode(',', Wallet::$types));
            $table->timestamps();

            $table->foreign(Wallet::USER_ID)->references(User::ID)->on(User::TABLE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Wallet::TABLE);
    }
}
