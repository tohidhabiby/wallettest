<?php

namespace Database\Seeders;

use App\Constants\PermissionTitle;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    protected $permissions = [
        PermissionTitle::GET_ALL_USERS,
        PermissionTitle::GET_USER,
        PermissionTitle::CREATE_USER,
        PermissionTitle::UPDATE_USER,
        PermissionTitle::DELETE_USER,
        PermissionTitle::GET_ALL_ROLES,
        PermissionTitle::GET_ROLE,
        PermissionTitle::CREATE_ROLE,
        PermissionTitle::UPDATE_ROLE,
        PermissionTitle::DELETE_ROLE,
        PermissionTitle::GET_ALL_PERMISSIONS,
        PermissionTitle::GET_PERMISSION,
        PermissionTitle::GET_ALL_RECORDS,
        PermissionTitle::GET_RECORD,
        PermissionTitle::CREATE_RECORD,
        PermissionTitle::UPDATE_RECORD,
        PermissionTitle::DELETE_RECORD,
        PermissionTitle::GET_ALL_WALLETS,
        PermissionTitle::GET_WALLET,
        PermissionTitle::CREATE_WALLET,
        PermissionTitle::UPDATE_WALLET,
        PermissionTitle::DELETE_WALLET,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::whereTitleLike(Role::CUSTOM_ROLE)->first();
        foreach ($this->permissions as $permission) {
            Permission::firstOrCreate([Permission::TITLE => $permission]);
        }
        $role->permissions()->sync(Permission::all());
    }
}
