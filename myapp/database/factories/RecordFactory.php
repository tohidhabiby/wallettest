<?php

namespace Database\Factories;

use App\Models\Model;
use App\Models\Record;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Record::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Record::AMOUNT => $this->faker->randomNumber(),
            Record::WALLET_ID => Wallet::factory()->create()->getId(),
            Record::TYPE => Record::$types[array_rand(Record::$types)],
        ];
    }
}
