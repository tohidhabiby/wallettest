<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Factories\Factory;

class WalletFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Wallet::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            Wallet::USER_ID => User::factory()->create()->getId(),
            Wallet::NAME => $this->faker->name,
            Wallet::TYPE => Wallet::$types[array_rand(Wallet::$types)],
        ];
    }
}
