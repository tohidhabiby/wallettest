<?php

namespace App\Constants;

class PermissionTitle
{
    const GET_ALL_RECORDS = 'GetAllRecords';
    const GET_RECORD = 'GetRecord';
    const CREATE_RECORD = 'CreateRecord';
    const UPDATE_RECORD = 'UpdateRecord';
    const DELETE_RECORD = 'DeleteRecord';

    const GET_ALL_WALLETS = 'GetAllWallets';
    const GET_WALLET = 'GetWallet';
    const CREATE_WALLET = 'CreateWallet';
    const UPDATE_WALLET = 'UpdateWallet';
    const DELETE_WALLET = 'DeleteWallet';

    const GET_ALL_USERS = 'GetAllUsers';
    const GET_USER = 'GetUser';
    const CREATE_USER = 'CreateUser';
    const UPDATE_USER = 'UpdateUser';
    const DELETE_USER = 'DeleteUser';

    const GET_ALL_ROLES = 'GetAllRoles';
    const GET_ROLE = 'GetRole';
    const CREATE_ROLE = 'CreateRole';
    const UPDATE_ROLE = 'UpdateRole';
    const DELETE_ROLE = 'DeleteRole';

    const GET_ALL_PERMISSIONS = 'GetAllPermissions';
    const GET_PERMISSION = 'GetPermission';
}
