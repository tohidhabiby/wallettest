<?php

namespace App\Rules;

use App\Interfaces\Models\UserInterface;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Contracts\Validation\Rule;

class CheckWalletRule implements Rule
{
    /**
     * @var User
     */
    protected $user;

    /**
     * Create a new rule instance.
     *
     * @param UserInterface $user User.
     */
    public function __construct(UserInterface $user)
    {
        $this->user = $user;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute Attribute.
     * @param  mixed  $value     Value.
     *
     * @return boolean
     */
    public function passes($attribute, $value) // phpcs:ignore
    {
        $wallet = Wallet::find($value);
        if (!$wallet || !$wallet->user->is($this->user)) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Wallet is not for you!';
    }
}
