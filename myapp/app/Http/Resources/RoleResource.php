<?php

namespace App\Http\Resources;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RoleResource
 *
 * @OA\Schema(
 *     schema="Role",
 *
 *     @OA\Property(property="id", format="int64", type="integer"),
 *     @OA\Property(property="title", type="string"),
 *     @OA\Property(property="permissions", ref="#/components/schemas/Permissions"),
 * )
 *
 * @OA\Schema(schema="Roles", type="array", @OA\Items(ref="#/components/schemas/Role"))
 *
 * @package App\Http\Resources
 */
class RoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request Request.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            Role::ID => $this->getId(),
            Role::TITLE => $this->getTitle(),
            'permissions' => $this->whenLoaded(
                'permissions',
                function () {
                    return PermissionResource::collection($this->permissions);
                }
            ),
        ];
    }
}
