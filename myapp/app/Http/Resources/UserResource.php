<?php

namespace App\Http\Resources;

use App\Constants\PermissionTitle;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserResource
 *
 * @OA\Schema(
 *     schema="User",
 *
 *     @OA\Property(property="id", format="int64", type="integer"),
 *     @OA\Property(property="first_name", type="string"),
 *     @OA\Property(property="last_name", type="string"),
 *     @OA\Property(property="charge", type="integer"),
 *     @OA\Property(property="email", type="string", example="tohidhabiby@gmail.com"),
 *     @OA\Property(property="created_at", type="string", example="2019-09-11 00:00:00"),
 *     @OA\Property(property="updated_at", type="string", example="2019-09-11 00:00:00"),
 *     @OA\Property(property="role", ref="#/components/schemas/Roles")
 * )
 *
 * @OA\Schema(schema="Users", type="array", @OA\Items(ref="#/components/schemas/User"))
 *
 * @package App\Http\Resources
 */
class UserResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param Request $request BaseRequest.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            User::ID => $this->getId(),
            User::FIRST_NAME => $this->getFirstName(),
            User::LAST_NAME => $this->getLastName(),
            User::EMAIL => $this->getEmail(),
            User::CHARGE => $this->getCharge(),
            User::CREATED_AT => $this->getCreatedAt(),
            User::UPDATED_AT => $this->getUpdatedAt(),
            'roles' => $this->when(
                $request->user()->hasPermission(PermissionTitle::CREATE_USER) ||
                        $request->user()->hasPermission(PermissionTitle::UPDATE_USER),
                        function () {
                            return RoleResource::collection($this->roles);
                        }
            ),
        ];
    }
}
