<?php

namespace App\Http\Resources;

use App\Models\Record;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class RecordResource
 *
 * @OA\Schema(
 *     schema="Record",
 *
 *     @OA\Property(property="id", format="int64", type="integer"),
 *     @OA\Property(property="amount", format="int64", type="integer"),
 *     @OA\Property(property="type", type="string", description="credit, debit"),
 *     @OA\Property(property="wallet_id", format="int64", type="integer"),
 *     @OA\Property(property="wallet", ref="#/components/schemas/Wallet"),
 *     @OA\Property(property="created_at", type="string", example="2019-09-11 00:00:00"),
 *     @OA\Property(property="updated_at", type="string", example="2019-09-11 00:00:00")
 * )
 *
 * @OA\Schema(schema="Records", type="array", @OA\Items(ref="#/components/schemas/Record"))
 *
 * @package App\Http\Resources
 *
 */
class RecordResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request Request.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            Record::ID => $this->getId(),
            Record::WALLET_ID => $this->getWalletId(),
            Record::AMOUNT => $this->getAmount(),
            Record::TYPE => $this->getType(),
            Record::CREATED_AT => $this->{Record::CREATED_AT},
            Record::UPDATED_AT => $this->{Record::UPDATED_AT},
            'wallet' => $this->whenLoaded(
                'wallet',
                function () {
                    return new WalletResource($this->wallet);
                }
            ),
        ];
    }
}
