<?php

namespace App\Http\Resources;

use App\Models\Wallet;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class WalletResource
 *
 * @OA\Schema(
 *     schema="Wallet",
 *
 *     @OA\Property(property="id", format="int64", type="integer"),
 *     @OA\Property(property="name", format="string"),
 *     @OA\Property(property="type", type="string", description="credit, cash"),
 *     @OA\Property(property="user_id", format="int64", type="integer"),
 *     @OA\Property(property="user", ref="#/components/schemas/User"),
 *     @OA\Property(property="created_at", type="string", example="2019-09-11 00:00:00"),
 *     @OA\Property(property="updated_at", type="string", example="2019-09-11 00:00:00")
 * )
 *
 * @OA\Schema(schema="Wallets", type="array", @OA\Items(ref="#/components/schemas/Wallet"))
 *
 * @package App\Http\Resources
 *
 */
class WalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request Request.
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            Wallet::ID => $this->getId(),
            Wallet::NAME => $this->getName(),
            Wallet::TYPE => $this->getType(),
            Wallet::USER_ID => $this->getUserId(),
            'user' => $this->whenLoaded(
                'user',
                function () {
                    return new UserResource($this->user);
                }
            ),
        ];
    }
}
