<?php

namespace App\Http\Resources;

use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PermissionResource
 *
 * @OA\Schema(
 *     schema="Permission",
 *
 *     @OA\Property(property="id", format="int64", type="integer"),
 *     @OA\Property(property="title", type="string")
 * )
 *
 * @OA\Schema(schema="Permissions", type="array", @OA\Items(ref="#/components/schemas/Permission"))
 *
 * @package App\Http\Resources
 */
class PermissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            Permission::ID => $this->getId(),
            Permission::TITLE => $this->getTitle(),
        ];
    }
}
