<?php

namespace App\Http\Middleware;

use App\Models\Permission;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class PermissionMiddleware
{
    /** @var Route */
    private $route;

    /**
     * CheckPermission constructor.
     *
     * @param Route $route Route.
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request     $request    Request.
     * @param Closure     $next       Closure.
     * @param string|null $permission Permission.
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $permission = null)
    {
        if ($permission === null) {
            $permission = Permission::generatePermissionTitle(
                $this->route->getActionMethod(),
                str_replace(
                    'Controller',
                    '',
                    substr(
                        strrchr(
                            get_class($this->route->controller),
                            '\\'
                        ),
                        1
                    )
                )
            );
        }

        abort_if(
            !($permission && $request->user()->hasPermission($permission)),
            403,
            __('You are not allowed to perform this action.')
        );

        return $next($request);
    }
}
