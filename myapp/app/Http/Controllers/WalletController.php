<?php

namespace App\Http\Controllers;

use App\Filters\WalletFilter;
use App\Http\Requests\WalletRequest;
use App\Http\Resources\WalletResource;
use App\Models\Wallet;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class WalletController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     * @OA\Get(
     *     path="/api/wallets",
     *     tags={"Wallets"},
     *     security={{"passport": {}}},
     *     operationId="GetAllWallets",
     *     description="Get All Wallets.",
     *     @OA\Parameter(
     *         description="name for filter Wallets by name",
     *         in="query",
     *         name="name",
     *         required=false,
     *         @OA\Schema(type="int")
     *     ),
     *     @OA\Parameter(
     *         description="Type for filter Wallets by type",
     *         in="query",
     *         name="type",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="IDs for filter Wallets by IDs",
     *         in="query",
     *         name="ids",
     *         required=false,
     *         @OA\Schema(type="int")
     *     ),
     *     @OA\Parameter(
     *         description="user for filter Wallets by userId",
     *         in="query",
     *         name="user",
     *         required=false,
     *         @OA\Schema(type="int")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="All wallets response",
     *         @OA\JsonContent(ref="#/components/schemas/Wallets")
     *     )
     * )
     *
     * @param WalletFilter $filters Filter.
     * @param Request      $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(WalletFilter $filters, Request $request): AnonymousResourceCollection
    {
        return WalletResource::collection(Wallet::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     path="/api/wallets",
     *     tags={"Wallets"},
     *     security={{"passport": {}}},
     *     operationId="CreateWallet",
     *     description="Create a new Wallet.",
     *     @OA\RequestBody(
     *         description="Wallet to add",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/WalletRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Wallet response",
     *         @OA\JsonContent(ref="#/components/schemas/Wallet")
     *     )
     * )
     *
     * @param WalletRequest $request Request.
     *
     * @return WalletResource
     */
    public function store(WalletRequest $request): WalletResource
    {
        return new WalletResource(
            Wallet::createObject(
                $request->user(),
                $request->get(Wallet::NAME),
                $request->get(Wallet::TYPE)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     path="/api/wallets/{wallet_id}",
     *     tags={"Wallets"},
     *     security={{"passport": {}}},
     *     operationId="GetWallet",
     *     description="Get Wallet.",
     *     @OA\Parameter(
     *         description="Wallet id which is going to fetch",
     *         in="path",
     *         name="wallet_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Wallet response",
     *         @OA\JsonContent(ref="#/components/schemas/Wallet")
     *     )
     * )
     *
     * @param Wallet $wallet Wallet.
     *
     * @return WalletResource
     */
    public function show(Wallet $wallet): WalletResource
    {
        return new WalletResource($wallet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Patch(
     *     path="/api/wallets/{wallet_id}",
     *     tags={"Wallets"},
     *     security={{"passport": {}}},
     *     operationId="UpdateWallet",
     *     description="Update a new Wallet.",
     *     @OA\Parameter(
     *         description="Wallet id which is going to update",
     *         in="path",
     *         name="wallet_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\RequestBody(
     *         description="Wallet to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/WalletRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Wallet response",
     *         @OA\JsonContent(ref="#/components/schemas/Wallet")
     *     )
     * )
     *
     * @param WalletRequest $request Request.
     * @param Wallet        $wallet  Wallet.
     *
     * @return WalletResource
     */
    public function update(WalletRequest $request, Wallet $wallet)
    {
        if (!$wallet->user->is($request->user())) {
            return response()
                ->json(['message' => 'This wallet is not for you!'], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $wallet->updateObject(
            $request->get(Wallet::NAME),
            $request->get(Wallet::TYPE)
        );

        return new WalletResource($wallet);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete(
     *     path="/api/wallets/{wallet_id}",
     *     tags={"Wallets"},
     *     security={{"passport": {}}},
     *     operationId="DeleteWallet",
     *     description="Delete Wallet.",
     *     @OA\Parameter(
     *         description="Wallet id which is going to fetch",
     *         in="path",
     *         name="wallet_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Wallet response",
     *         @OA\JsonContent(ref="#/components/schemas/Wallet")
     *     )
     * )
     *
     * @param Wallet $wallet Wallet.
     *
     * @return JsonResponse
     */
    public function destroy(Wallet $wallet): JsonResponse
    {
        try {
            $wallet->delete();

            return response()->json([], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
    }
}
