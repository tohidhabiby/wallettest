<?php

namespace App\Http\Controllers;

use App\Constants\PermissionTitle;
use App\Filters\RoleFilter;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\RoleResource;
use App\Models\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     path="/api/roles",
     *     tags={"Roles"},
     *     security={{"passport": {}}},
     *     operationId="GetAllRoles",
     *     description="Get All Roles.",
     *     @OA\Parameter(
     *         description="Title for filter role by title like",
     *         in="query",
     *         name="title",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="All Roles response",
     *         @OA\JsonContent(ref="#/components/schemas/Roles")
     *     )
     * )
     *
     * @param RoleFilter $filters Filter.
     * @param Request    $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(RoleFilter $filters, Request $request): AnonymousResourceCollection
    {
        return RoleResource::collection(Role::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     path="/api/roles",
     *     tags={"Roles"},
     *     security={{"passport": {}}},
     *     operationId="CreateRole",
     *     description="Create a new Role.",
     *     @OA\RequestBody(
     *         description="Role to add",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/RoleRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Role response",
     *         @OA\JsonContent(ref="#/components/schemas/Role")
     *     )
     * )
     *
     * @param RoleRequest $request BaseRequest.
     *
     * @return RoleResource
     */
    public function store(RoleRequest $request): RoleResource
    {
        $role = Role::createObject($request->get(Role::TITLE));
        $role->permissions()->sync($request->get('permissions'));
        return new RoleResource($role);
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     path="/api/roles/{role_id}",
     *     tags={"Roles"},
     *     security={{"passport": {}}},
     *     operationId="GetRole",
     *     description="Get Role.",
     *     @OA\Parameter(
     *         description="Role id which is going to fetch",
     *         in="path",
     *         name="role_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Role response",
     *         @OA\JsonContent(ref="#/components/schemas/Role")
     *     )
     * )
     *
     * @param Role $role Role.
     *
     * @return RoleResource
     */
    public function show(Role $role): RoleResource
    {
        $user = Auth::user();
        if ($user->hasPermission(PermissionTitle::GET_ALL_PERMISSIONS)) {
            return new RoleResource($role->load('permissions'));
        }

        return new RoleResource($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Patch(
     *     path="/api/roles/{role_id}",
     *     tags={"Roles"},
     *     security={{"passport": {}}},
     *     operationId="UpdateRole",
     *     description="Update a new Role.",
     *     @OA\Parameter(
     *         description="Role id which is going to update",
     *         in="path",
     *         name="role_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\RequestBody(
     *         description="Role to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/RoleRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Role response",
     *         @OA\JsonContent(ref="#/components/schemas/Role")
     *     )
     * )
     *
     * @param RoleRequest $request BaseRequest.
     *
     * @param Role        $role    Role.
     *
     * @return RoleResource
     */
    public function update(RoleRequest $request, Role $role): RoleResource
    {
        $role->updateObject($request->get(Role::TITLE));
        $role->permissions()->sync($request->get('permissions'));
        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete(
     *     path="/api/roles/{role_id}",
     *     tags={"Roles"},
     *     security={{"passport": {}}},
     *     operationId="DeleteRole",
     *     description="Delete Role.",
     *     @OA\Parameter(
     *         description="Role id which is going to fetch",
     *         in="path",
     *         name="role_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Role response",
     *         @OA\JsonContent(ref="#/components/schemas/Role")
     *     )
     * )
     *
     * @param Role $role Role.
     *
     * @return JsonResponse
     */
    public function destroy(Role $role): JsonResponse
    {
        try {
            $role->delete();

            return response()->json([], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
    }
}
