<?php

namespace App\Http\Controllers;

use App\Filters\PermissionFilter;
use App\Http\Resources\PermissionResource;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     path="/api/permissions",
     *     tags={"Permissions"},
     *     security={{"passport": {}}},
     *     operationId="GetAllPermissions",
     *     description="Get All Permissions.",
     *     @OA\Parameter(
     *         description="Title for filter permission by title like",
     *         in="query",
     *         name="title",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="All Permissions response",
     *         @OA\JsonContent(ref="#/components/schemas/Permissions")
     *     )
     * )
     *
     * @param PermissionFilter $filters Filter.
     *
     * @param Request          $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(PermissionFilter $filters, Request $request): AnonymousResourceCollection
    {
        return PermissionResource::collection(Permission::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * * Display the specified resource.
     *
     * @OA\Get(
     *     path="/api/permissions/{permission_id}",
     *     tags={"Permissions"},
     *     security={{"passport": {}}},
     *     operationId="GetPermission",
     *     description="Get Permission.",
     *     @OA\Parameter(
     *         description="Permission id which is going to fetch",
     *         in="path",
     *         name="permission_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Permission response",
     *         @OA\JsonContent(ref="#/components/schemas/Permission")
     *     )
     * )
     * @param Permission $permission Permission.
     *
     * @return PermissionResource
     */
    public function show(Permission $permission): PermissionResource
    {
        return new PermissionResource($permission);
    }
}
