<?php

namespace App\Http\Controllers;

use App\Filters\RecordFilter;
use App\Http\Requests\RecordRequest;
use App\Http\Resources\RecordResource;
use App\Models\Record;
use App\Models\Wallet;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     * @OA\Get(
     *     path="/api/records",
     *     tags={"Records"},
     *     security={{"passport": {}}},
     *     operationId="GetAllRecords",
     *     description="Get All Records.",
     *     @OA\Parameter(
     *         description="Wallet for filter Records by walletId",
     *         in="query",
     *         name="wallet",
     *         required=false,
     *         @OA\Schema(type="int")
     *     ),
     *     @OA\Parameter(
     *         description="Type for filter Records by type",
     *         in="query",
     *         name="type",
     *         required=false,
     *         @OA\Schema(type="string")
     *     ),
     *     @OA\Parameter(
     *         description="IDs for filter Records by IDs",
     *         in="query",
     *         name="ids",
     *         required=false,
     *         @OA\Schema(type="int")
     *     ),
     *     @OA\Parameter(
     *         description="Amount for filter Records by Amount",
     *         in="query",
     *         name="amount",
     *         required=false,
     *         @OA\Schema(type="int")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="All records response",
     *         @OA\JsonContent(ref="#/components/schemas/Records")
     *     )
     * )
     *
     * @param RecordFilter $filters Filter.
     * @param Request      $request Request.
     *
     * @return AnonymousResourceCollection
     */
    public function index(RecordFilter $filters, Request $request): AnonymousResourceCollection
    {
        return RecordResource::collection(Record::filter($filters)->paginate($this->getPageSize($request)));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     path="/api/records",
     *     tags={"Records"},
     *     security={{"passport": {}}},
     *     operationId="CreateRecord",
     *     description="Create a new Record.",
     *     @OA\RequestBody(
     *         description="Record to add",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/RecordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Record response",
     *         @OA\JsonContent(ref="#/components/schemas/Record")
     *     )
     * )
     *
     * @param RecordRequest $request Request.
     *
     * @return RecordResource
     */
    public function store(RecordRequest $request): RecordResource
    {
        $wallet = Wallet::find($request->get(Record::WALLET_ID));

        return new RecordResource(
            Record::createObject(
                $wallet,
                $request->get(Record::AMOUNT),
                $request->get(Record::TYPE)
            )
        );
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     path="/api/records/{record_id}",
     *     tags={"Records"},
     *     security={{"passport": {}}},
     *     operationId="GetRecord",
     *     description="Get Record.",
     *     @OA\Parameter(
     *         description="Record id which is going to fetch",
     *         in="path",
     *         name="record_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Record response",
     *         @OA\JsonContent(ref="#/components/schemas/Record")
     *     )
     * )
     *
     * @param Record $record Record.
     *
     * @return RecordResource
     */
    public function show(Record $record): RecordResource
    {
        return new RecordResource($record);
    }

    /**
     * Update the specified resource in storage.
     *
     * @OA\Patch(
     *     path="/api/records/{record_id}",
     *     tags={"Records"},
     *     security={{"passport": {}}},
     *     operationId="UpdateRecord",
     *     description="Update a new Record.",
     *     @OA\Parameter(
     *         description="Record id which is going to update",
     *         in="path",
     *         name="record_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\RequestBody(
     *         description="Record to update",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/RecordRequest")
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Record response",
     *         @OA\JsonContent(ref="#/components/schemas/Record")
     *     )
     * )
     *
     * @param RecordRequest $request Request.
     * @param Record        $record  Record.
     *
     * @return RecordResource
     */
    public function update(RecordRequest $request, Record $record)
    {
        $wallet = Wallet::find($request->get(Record::WALLET_ID));

        $record->updateObject(
            $wallet,
            $request->get(Record::AMOUNT),
            $request->get(Record::TYPE)
        );

        return new RecordResource($record);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @OA\Delete(
     *     path="/api/records/{record_id}",
     *     tags={"Records"},
     *     security={{"passport": {}}},
     *     operationId="DeleteRecord",
     *     description="Delete Record.",
     *     @OA\Parameter(
     *         description="Record id which is going to fetch",
     *         in="path",
     *         name="record_id",
     *         required=true,
     *         @OA\Schema(type="integer", format="int64")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Record response",
     *         @OA\JsonContent(ref="#/components/schemas/Record")
     *     )
     * )
     *
     * @param Record $record Record.
     *
     * @return JsonResponse
     */
    public function destroy(Record $record): JsonResponse
    {
        try {
            $record->delete();

            return response()->json([], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], Response::HTTP_CONFLICT);
        }
    }
}
