<?php

namespace App\Http\Requests;

use App\Models\Role;

/**
 * Class RoleRequest
 *
 * @OA\Schema(
 *   schema="RoleRequest",
 *   type="object",
 *   required={"title"},
 *   @OA\Property(property="title", type="string"),
 * )
 *
 * @package App\Http\Requests
 */
class RoleRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Role::TITLE => sprintf(
                'required|unique:%s,%s,%s',
                Role::TABLE,
                Role::TITLE,
                optional($this->role)->getId()
            )
        ];
    }
}
