<?php

namespace App\Http\Requests;

use App\Models\User;

/**
 * Class UserRequest
 * @package App\Http\Requests
 *
 * @OA\Schema(
 *   schema="UserRequest",
 *   type="object",
 *   required={"email", "last_name", "first_name"},
 *   @OA\Property(property="first_name", type="string"),
 *   @OA\Property(property="last_name", type="string"),
 *   @OA\Property(property="password", type="string"),
 *   @OA\Property(property="email", type="string"),
 *   @OA\Property(property="charge", type="integer"),
 * )
 */
class UserRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            User::EMAIL => sprintf(
                'required|unique:%s,%s,%s',
                User::TABLE,
                User::EMAIL,
                optional($this->user)->{User::ID}
            ),
            User::FIRST_NAME => 'required|string|max:150',
            User::LAST_NAME => 'required|string',
            User::CHARGE => 'nullable|numeric',
            User::PASSWORD => 'nullable',
        ];
    }
}
