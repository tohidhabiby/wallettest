<?php

namespace App\Http\Requests;

use App\Models\Record;
use App\Models\Wallet;
use App\Rules\CheckWalletRule;

/**
 * Class RecordRequest
 *
 *  * @OA\Schema(
 *   schema="RecordRequest",
 *   type="object",
 *   required={"type", "amount", "wallet_id"},
 *   @OA\Property(property="wallet_id", type="int"),
 *   @OA\Property(property="type", type="string", description="debit, credit"),
 *   @OA\Property(property="amount", type="int"),
 * )
 *
 * @package App\Http\Requests
 */
class RecordRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Record::TYPE => 'required|in:' . implode(',', Record::$types),
            Record::AMOUNT => 'required|numeric',
            Record::WALLET_ID => [
                'required',
                'numeric',
                'exists:' . Wallet::TABLE . ',' . Wallet::ID,
                new CheckWalletRule($this->user()),
            ]
        ];
    }
}
