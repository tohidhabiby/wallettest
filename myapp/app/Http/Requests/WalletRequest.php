<?php

namespace App\Http\Requests;

use App\Models\Wallet;

/**
 * Class WalletRequest
 *
 *  * @OA\Schema(
 *   schema="WalletRequest",
 *   type="object",
 *   required={"type", "name"},
 *   @OA\Property(property="name", type="string"),
 *   @OA\Property(property="type", type="string", description="credit, cash"),
 * )
 *
 * @package App\Http\Requests
 */
class WalletRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Wallet::TYPE => 'required|in:' . implode(',', Wallet::$types),
            Wallet::NAME => 'required|string',
        ];
    }
}
