<?php

namespace App\Traits;

use App\Models\Wallet;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasWalletIdTrait
{
    /**
     * @param Builder $builder  Builder.
     * @param integer $walletId Wallet ID.
     *
     * @return Builder
     */
    public function scopeWhereWalletIdIs(Builder $builder, int $walletId): Builder
    {
        return $builder->where(self::WALLET_ID, $walletId);
    }

    /**
     * @return BelongsTo
     */
    public function wallet(): BelongsTo
    {
        return $this->belongsTo(Wallet::class);
    }
}
