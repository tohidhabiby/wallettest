<?php

namespace App\Traits\Filters;

use Illuminate\Database\Eloquent\Builder;

trait FilterWalletTrait
{
    /**
     * Filter by name.
     *
     * @param integer $walletId Wallet ID.
     *
     * @return Builder
     */
    protected function wallet(int $walletId): Builder
    {
        return $this->builder->whereWalletIdIs($walletId);
    }
}
