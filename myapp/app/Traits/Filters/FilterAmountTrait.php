<?php

namespace App\Traits\Filters;

use Illuminate\Database\Eloquent\Builder;

trait FilterAmountTrait
{
    /**
     * Filter by amount.
     *
     * @param integer $amount Amount.
     *
     * @return Builder
     */
    protected function amount(int $amount): Builder
    {
        return $this->builder->whereAmountIs($amount);
    }
}
