<?php

namespace App\Traits\Filters;

use Illuminate\Database\Eloquent\Builder;

trait FilterUserTrait
{
    /**
     * Filter by user.
     *
     * @param integer $userId User ID.
     *
     * @return Builder
     */
    protected function user(int $userId): Builder
    {
        return $this->builder->whereWalletIdIs($userId);
    }
}
