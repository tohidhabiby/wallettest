<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait HasLastNameTrait
{
    /**
     * @param Builder $builder  Builder.
     * @param string  $lastName LastName.
     *
     * @return Builder
     */
    public function scopeWhereLastNameLike(Builder $builder, string $lastName): Builder
    {
        return $builder->where(self::LAST_NAME, 'like', "%$lastName%");
    }
}
