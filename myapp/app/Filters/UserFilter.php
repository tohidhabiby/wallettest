<?php

namespace App\Filters;

use App\Traits\Filters\FilterIdsTrait;
use Illuminate\Database\Eloquent\Builder;

class UserFilter extends Filters
{
    use FilterIdsTrait;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'firstName',
        'lastName',
        'nationalCode',
        'type',
        'email',
        'mobile',
        'approved',
        'city',
        'region',
        'ids',
    ];

    /**
     * @param string $firstName FirstName.
     *
     * @return Builder
     */
    protected function firstName(string $firstName): Builder
    {
        return $this->builder->whereFirstNameLike($firstName);
    }

    /**
     * @param string $lastName Last Name.
     *
     * @return Builder
     */
    protected function lastName(string $lastName): Builder
    {
        return $this->builder->whereLastNameLike($lastName);
    }

    /**
     * @param string $nationalCode National Code.
     *
     * @return Builder
     */
    protected function nationalCode(string $nationalCode): Builder
    {
        return $this->builder->whereNationalCodeLike($nationalCode);
    }

    /**
     * @param string $type Type.
     *
     * @return Builder
     */
    protected function type(string $type): Builder
    {
        return $this->builder->whereTypeIs($type);
    }

    /**
     * @param string $email Email.
     *
     * @return Builder
     */
    protected function email(string $email): Builder
    {
        return $this->builder->whereEmailLike($email);
    }

    /**
     * @param string $mobile Mobile.
     *
     * @return Builder
     */
    protected function mobile(string $mobile): Builder
    {
        return $this->builder->whereMobileIs($mobile);
    }

    /**
     * @param boolean $approved Approved.
     *
     * @return Builder Builder.
     */
    protected function approved(bool $approved): Builder
    {
        return $this->builder->whereApprovedIs($approved);
    }

    /**
     * @param string $city City.
     *
     * @return Builder
     */
    protected function city(string $city): Builder
    {
        return $this->builder->whereCityLike($city);
    }

    /**
     * @param string $region Region.
     *
     * @return Builder
     */
    protected function region(string $region): Builder
    {
        return $this->builder->whereRegionLike($region);
    }
}
