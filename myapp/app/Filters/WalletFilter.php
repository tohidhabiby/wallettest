<?php

namespace App\Filters;

use App\Traits\Filters\FilterIdsTrait;
use App\Traits\Filters\FilterNameTrait;
use App\Traits\Filters\FilterTypeTrait;
use App\Traits\Filters\FilterUserTrait;

class WalletFilter extends Filters
{
    use FilterIdsTrait;
    use FilterTypeTrait;
    use FilterUserTrait;
    use FilterNameTrait;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'type',
        'ids',
        'user',
        'name',
    ];
}
