<?php

namespace App\Filters;

use App\Traits\Filters\FilterAmountTrait;
use App\Traits\Filters\FilterIdsTrait;
use App\Traits\Filters\FilterTypeTrait;
use App\Traits\Filters\FilterWalletTrait;

class RecordFilter extends Filters
{
    use FilterIdsTrait;
    use FilterTypeTrait;
    use FilterWalletTrait;
    use FilterAmountTrait;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'type',
        'ids',
        'amount',
        'wallet',
    ];
}
