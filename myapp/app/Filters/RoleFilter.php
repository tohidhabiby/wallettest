<?php

namespace App\Filters;

use App\Traits\Filters\FilterIdsTrait;
use App\Traits\Filters\FilterTitleTrait;

class RoleFilter extends Filters
{
    use FilterIdsTrait;
    use FilterTitleTrait;

    /**
     * Registered filters to operate upon.
     *
     * @var array
     */
    protected $filters = [
        'title',
        'ids',
    ];
}
