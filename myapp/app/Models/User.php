<?php

namespace App\Models;

use App\Filters\UserFilter;
use App\Interfaces\Models\UserInterface;
use App\Traits\HasCityTrait;
use App\Traits\HasApprovedTrait;
use App\Traits\HasEmailTrait;
use App\Traits\HasIdTrait;
use App\Traits\HasMobileTrait;
use App\Traits\HasNationalCodeTrait;
use App\Traits\HasFirstNameTrait;
use App\Traits\HasLastNameTrait;
use App\Traits\HasRegionTrait;
use App\Traits\HasTypeTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements UserInterface
{
    use HasFactory;
    use Notifiable;
    use HasApiTokens;
    use HasFirstNameTrait;
    use HasLastNameTrait;
    use MagicMethodsTrait;
    use HasEmailTrait;
    use HasIdTrait;

    const TABLE = 'users';
    const ID = 'id';
    const EMAIL = 'email';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const CHARGE = 'charge';
    const PASSWORD = 'password';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Filter scope.
     *
     * @param Builder    $builder Builder.
     * @param UserFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, UserFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIRST_NAME,
        self::LAST_NAME,
        self::EMAIL,
        self::CHARGE,
        self::PASSWORD,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD
    ];

    /**
     * Create new user.
     *
     * @param string      $firstName First name.
     * @param string      $lastName  Last name.
     * @param string      $password  Password.
     * @param string      $email     Email.
     * @param string|null $charge    Charge for using Mobin panel.
     * @return UserInterface
     */
    public static function createObject(
        string $firstName,
        string $lastName,
        string $password,
        string $email,
        ?string $charge
    ): UserInterface {
        $user = new static();
        $user->setFirstName($firstName);
        $user->setLastName($lastName);
        $user->setPassword(Hash::make($password));
        $user->setEmail($email);
        if (!is_null($charge)) {
            $user->setCharge($charge);
        }
        $user->save();

        return $user;
    }

    /**
     * update existing user.
     *
     * @param string      $firstName FirstName.
     * @param string      $lastName  LastName.
     * @param string|null $password  Password.
     * @param string|null $email     Email.
     * @param string|null $charge    Charge.
     *
     * @return UserInterface
     */
    public function updateObject(
        string $firstName,
        ?string $lastName,
        ?string $password,
        ?string $email,
        ?string $charge
    ): UserInterface {
        $this->setFirstName($firstName);
        $this->setLastName($lastName);
        if (!is_null($password)) {
            $this->setPassword($password);
        }
        if (!is_null($email)) {
            $this->setEmail($email);
        }
        if (!is_null($charge)) {
            $this->setCharge($charge);
        }
        $this->save();

        return $this;
    }

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * User has permission.
     *
     * @param string $permission Permission title.
     *
     * @return boolean
     */
    public function hasPermission(string $permission): bool
    {
        return !empty(static::whereIdIn([$this->getId()])->whereHasPermission($permission)->first());
    }

    /**
     * @param Builder $builder    Builder.
     * @param string  $permission Permission title.
     *
     * @return Builder
     */
    public function scopeWhereHasPermission(Builder $builder, string $permission): Builder
    {
        return $builder->whereHas('roles', function (Builder $joinRole) use ($permission) {
            return $joinRole->whereHas('permissions', function (Builder $joinPermission) use ($permission) {
                return $joinPermission->whereTitleIs($permission);
            });
        });
    }
}
