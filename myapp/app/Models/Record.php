<?php

namespace App\Models;

use App\Filters\RecordFilter;
use App\Interfaces\Models\RecordInterface;
use App\Interfaces\Models\WalletInterface;
use App\Traits\HasAmountTrait;
use App\Traits\HasIdTrait;
use App\Traits\HasTypeTrait;
use App\Traits\HasWalletIdTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Record extends Model implements RecordInterface
{
    use HasFactory;
    use HasIdTrait;
    use HasTypeTrait;
    use HasAmountTrait;
    use MagicMethodsTrait;
    use HasWalletIdTrait;

    const TABLE = 'records';
    const ID = 'id';
    const AMOUNT = 'amount';
    const TYPE = 'type';
    const WALLET_ID = 'wallet_id';
    const TYPE_DEBIT = 'debit';
    const TYPE_CREDIT = 'credit';

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_CREDIT,
        self::TYPE_DEBIT,
    ];

    /**
     * Filter scope.
     *
     * @param Builder      $builder Builder.
     * @param RecordFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, RecordFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * @param WalletInterface $wallet Wallet.
     * @param integer         $amount Amount.
     * @param string          $type   Type.
     *
     * @return RecordInterface
     */
    public static function createObject(
        WalletInterface $wallet,
        int $amount,
        string $type
    ): RecordInterface {
        $record = new static();
        $record->setWalletId($wallet->getId());
        $record->setAmount($amount);
        $record->setType($type);
        $record->save();

        return $record;
    }

    /**
     * @param WalletInterface $wallet Wallet.
     * @param integer         $amount Amount.
     * @param string          $type   Type.
     *
     * @return RecordInterface
     */
    public function updateObject(
        WalletInterface $wallet,
        int $amount,
        string $type
    ): RecordInterface {
        $this->setWalletId($wallet->getId());
        $this->setAmount($amount);
        $this->setType($type);
        $this->save();

        return $this;
    }
}
