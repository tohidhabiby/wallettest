<?php

namespace App\Models;

use App\Filters\PermissionFilter;
use App\Traits\HasTitleTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class Permission extends Model
{
    use HasFactory;
    use MagicMethodsTrait;
    use HasTitleTrait;

    const TABLE = 'permissions';
    const ID = 'id';
    const TITLE = 'title';

    /**
     * @var boolean
     */
    public $timestamps = false;

    /**
     * @param Builder          $builder Builder.
     * @param PermissionFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, PermissionFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * Generate permission title.
     *
     * @param string      $methodName Method name.
     * @param string|null $slug       Slug.
     *
     * @return string|null
     */
    public static function generatePermissionTitle(string $methodName, ?string $slug = null): ?string
    {
        if ($slug === null) {
            return null;
        }

        switch ($methodName) {
            case 'index':
                $permission = 'GetAll' . Str::plural($slug);
                break;

            case 'show':
                $permission = 'Get' . $slug;
                break;

            case 'store':
                $permission = 'Create' . $slug;
                break;

            case 'destroy':
                $permission = 'Delete' . $slug;
                break;

            case 'update':
                $permission = 'Update' . $slug;
                break;

            default:
                return null;
        }

        return $permission;
    }

    /**
     * @return BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class);
    }
}
