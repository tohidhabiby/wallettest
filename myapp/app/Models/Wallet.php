<?php

namespace App\Models;

use App\Filters\WalletFilter;
use App\Interfaces\Models\UserInterface;
use App\Interfaces\Models\WalletInterface;
use App\Traits\HasIdTrait;
use App\Traits\HasNameTrait;
use App\Traits\HasTypeTrait;
use App\Traits\MagicMethodsTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Wallet extends Model implements WalletInterface
{
    use HasFactory;
    use HasIdTrait;
    use MagicMethodsTrait;
    use HasTypeTrait;
    use HasNameTrait;

    const TABLE = 'wallets';
    const ID = 'id';
    const USER_ID = 'user_id';
    const NAME = 'name';
    const TYPE = 'type';
    const TYPE_CREDIT = 'credit';
    const TYPE_CASH = 'cash';

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_CASH,
        self::TYPE_CREDIT,
    ];

    /**
     * Filter scope.
     *
     * @param Builder      $builder Builder.
     * @param WalletFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, WalletFilter $filters): Builder
    {
        return $filters->apply($builder);
    }

    /**
     * @param UserInterface $user User.
     * @param string        $name Name.
     * @param string        $type Type.
     *
     * @return WalletInterface
     */
    public static function createObject(
        UserInterface $user,
        string $name,
        string $type
    ): WalletInterface {
        $wallet = new static();
        $wallet->setUserId($user->getId());
        $wallet->setName($name);
        $wallet->setType($type);
        $wallet->save();

        return $wallet;
    }

    /**
     * @param string $name Name.
     * @param string $type Type.
     *
     * @return WalletInterface
     */
    public function updateObject(string $name, string $type): WalletInterface
    {
        $this->setName($name);
        $this->setType($type);
        $this->save();

        return $this;
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
