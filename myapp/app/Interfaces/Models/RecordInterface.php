<?php

namespace App\Interfaces\Models;

use App\Filters\RecordFilter;
use App\Interfaces\Traits\HasAmountInterface;
use App\Interfaces\Traits\HasIdInterface;
use App\Interfaces\Traits\HasTypeInterface;
use App\Interfaces\Traits\HasWalletIdInterface;
use Illuminate\Database\Eloquent\Builder;

interface RecordInterface extends
    HasIdInterface,
    HasTypeInterface,
    HasAmountInterface,
    HasWalletIdInterface
{
    /**
     * Filter scope.
     *
     * @param Builder      $builder Builder.
     * @param RecordFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, RecordFilter $filters): Builder;

    /**
     * @param WalletInterface $wallet Wallet.
     * @param integer         $amount Amount.
     * @param string          $type   Type.
     *
     * @return RecordInterface
     */
    public static function createObject(
        WalletInterface $wallet,
        int $amount,
        string $type
    ): RecordInterface;

    /**
     * @param WalletInterface $wallet Wallet.
     * @param integer         $amount Amount.
     * @param string          $type   Type.
     *
     * @return RecordInterface
     */
    public function updateObject(
        WalletInterface $wallet,
        int $amount,
        string $type
    ): RecordInterface;
}
