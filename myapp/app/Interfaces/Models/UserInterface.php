<?php

namespace App\Interfaces\Models;

use App\Interfaces\Traits\HasEmailInterface;
use App\Interfaces\Traits\HasFirstNameInterface;
use App\Interfaces\Traits\HasIdInterface;
use App\Interfaces\Traits\HasLastNameInterface;
use Illuminate\Database\Eloquent\Builder;

interface UserInterface extends
    HasFirstNameInterface,
    HasLastNameInterface,
    HasEmailInterface,
    HasIdInterface
{
    /**
     * Create new user.
     *
     * @param string      $firstName First name.
     * @param string      $lastName  Last name.
     * @param string      $password  Password.
     * @param string      $email     Email.
     * @param string|null $charge    Charge for using Mobin panel.
     * @return UserInterface
     */
    public static function createObject(
        string $firstName,
        string $lastName,
        string $password,
        string $email,
        ?string $charge
    );

    /**
     * update existing user.
     *
     * @param string      $firstName FirstName.
     * @param string      $lastName  LastName.
     * @param string|null $password  Password.
     * @param string|null $email     Email.
     * @param string|null $charge    Charge.
     *
     * @return UserInterface
     */
    public function updateObject(
        string $firstName,
        ?string $lastName,
        ?string $password,
        ?string $email,
        ?string $charge
    ): UserInterface;

    /**
     * @param string $permission Permission.
     *
     * @return boolean
     */
    public function hasPermission(string $permission): bool;

    /**
     * @param Builder $builder    Builder.
     * @param string  $permission Permission.
     *
     * @return Builder
     */
    public function scopeWhereHasPermission(Builder $builder, string $permission): Builder;
}
