<?php

namespace App\Interfaces\Models;

use App\Filters\WalletFilter;
use App\Interfaces\Traits\HasIdInterface;
use App\Interfaces\Traits\HasNameInterface;
use App\Interfaces\Traits\HasTypeInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface WalletInterface extends
    HasIdInterface,
    HasTypeInterface,
    HasNameInterface
{
    /**
     * Filter scope.
     *
     * @param Builder      $builder Builder.
     * @param WalletFilter $filters Filters.
     *
     * @return Builder
     */
    public function scopeFilter(Builder $builder, WalletFilter $filters): Builder;

    /**
     * @param UserInterface $user User.
     * @param string        $name Name.
     * @param string        $type Type.
     *
     * @return WalletInterface
     */
    public static function createObject(
        UserInterface $user,
        string $name,
        string $type
    ): WalletInterface;

    /**
     * @param string $name Name.
     * @param string $type Type.
     *
     * @return WalletInterface
     */
    public function updateObject(string $name, string $type): WalletInterface;

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo;
}
