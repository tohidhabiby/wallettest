<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasFirstNameInterface
{
    /**
     * @param Builder $builder   Builder.
     * @param string  $firstName FirstName.
     *
     * @return Builder
     */
    public function scopeWhereFirstNameLike(Builder $builder, string $firstName): Builder;
}
