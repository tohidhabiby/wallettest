<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasLastNameInterface
{
    /**
     * @param Builder $builder  Builder.
     * @param string  $lastName LastName.
     *
     * @return Builder
     */
    public function scopeWhereLastNameLike(Builder $builder, string $lastName): Builder;
}
