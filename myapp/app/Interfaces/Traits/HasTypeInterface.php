<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasTypeInterface
{
    /**
     * @param Builder $builder Builder.
     * @param string  $type    Type.
     *
     * @return Builder
     */
    public function scopeWhereTypeIs(Builder $builder, string $type): Builder;
}
