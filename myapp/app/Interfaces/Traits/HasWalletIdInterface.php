<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

interface HasWalletIdInterface
{
    /**
     * @param Builder $builder  Builder.
     * @param integer $walletId Wallet ID.
     *
     * @return Builder
     */
    public function scopeWhereWalletIdIs(Builder $builder, int $walletId): Builder;

    /**
     * @return BelongsTo
     */
    public function wallet(): BelongsTo;
}
