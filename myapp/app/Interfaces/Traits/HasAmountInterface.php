<?php

namespace App\Interfaces\Traits;

use Illuminate\Database\Eloquent\Builder;

interface HasAmountInterface
{
    /**
     * @param Builder $builder Builder.
     * @param integer $amount  Amount.
     *
     * @return Builder
     */
    public function scopeWhereAmountIs(Builder $builder, int $amount): Builder;
}
